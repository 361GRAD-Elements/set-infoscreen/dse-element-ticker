<?php

/**
 * 361GRAD-Element - Ticker
 *
 * @package   dse-elements-bundle
 * @author    Markus Häfner <markus@361.de>
 * @copyright 2018 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_ticker']     = ['Ticker', 'Marquee element.'];

$GLOBALS['TL_LANG']['tl_content']['ticker_legend'] = 'Ticker';
$GLOBALS['TL_LANG']['tl_content']['dse_tickerfields']     = ['Ticker-Fields', 'Add any number of fields.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
