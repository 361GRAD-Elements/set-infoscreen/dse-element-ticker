<?php

/**
 * 361GRAD-Element - Ticker
 *
 * @package   dse-elements-bundle
 * @author    Markus Häfner <markus@361.de>
 * @copyright 2018 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements'] = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_ticker']     = ['Ticker', 'Laufschrift-Element.'];

$GLOBALS['TL_LANG']['tl_content']['ticker_legend'] = 'Ticker';
$GLOBALS['TL_LANG']['tl_content']['dse_tickerfields']     = ['Ticker-Felder', 'Unbestimmte Anzahl an Felder hinzufügen.'];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Abstandeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];
