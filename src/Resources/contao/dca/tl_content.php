<?php

/**
 * 361GRAD-Element - Ticker
 *
 * @package   dse-elements-bundle
 * @author    Markus Häfner <markus@361.de>
 * @copyright 2018 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_ticker'] =
    '{type_legend},type,headline;' .
    '{ticker_legend},dse_tickerfields;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{invisible_legend:hide},invisible,start,stop';

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tickerfields'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_tickerfields'],
    'exclude'   => true,
    'search'    => true,
    'inputType' => 'listWizard',
    'fields' => array(
        'text_field' => array(
            'label' => array(
                'de' => array(
                    'Text'
                ),
                'en' => array(
                    'Text'
                ),
            ),
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength'=>255,
                'tl_class' => 'clr w100'
            )
        ),
    ),
    'eval' => array('tl_class'=>'clr'),
    'sql' => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];
