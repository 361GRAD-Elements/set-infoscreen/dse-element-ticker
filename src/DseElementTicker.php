<?php

/**
 * 361GRAD-Element - Ticker
 *
 * @package   dse-elements-bundle
 * @author    Markus Häfner <markus@361.de>
 * @copyright 2018 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementTicker;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD-Element - List.
 */
class DseElementTicker extends Bundle
{
}
